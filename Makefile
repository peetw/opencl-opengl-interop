# Project name
PROJECT = opencl-opengl-interop

# Main directories
DIR_BIN = bin
DIR_OBJ = obj
DIR_SRC = src

# Directories to create if not already existant
MKDIR := $(DIR_BIN) $(DIR_OBJ)

# Binary file
BIN := $(DIR_BIN)/$(PROJECT)

# C++ source files
SRC := $(wildcard $(DIR_SRC)/*.cpp)
OBJ := $(patsubst %.cpp,%.o,$(subst $(DIR_SRC),$(DIR_OBJ),$(SRC)))

# Dependency file
DEP = .depend

# Compiler options
CXXFLAGS = -c -std=c++11 -O3 -march=native -mtune=native -Wall -Werror -Wno-comment
INCLUDES = -I/opt/AMDAPP/include

# Linker options
#LDFLAGS = -Wl,--verbose
LDPATHS = -L/opt/AMDAPP/lib/x86_64
LDLIBS = -lglut -lGL -lGLU -lGLEW -lOpenCL

# Non-file targets
.PHONY: all debug clean

# Default target
all: $(MKDIR) $(BIN)

# Debug target
debug: CXXFLAGS := -g $(filter-out -O%, $(CXXFLAGS))
debug: all

# Create directories
$(MKDIR):
	mkdir -p $@

# Create binary
$(BIN): $(OBJ)
	@$(RM) $(BIN)
	$(CXX) $(LDFLAGS) $^ $(LDPATHS) $(LDLIBS) -o $@
	@echo "\n**** Compiled on: "`date`" ****"

# Determine C++ dependices
$(DEP): $(SRC)
	@$(CXX) -xc++ $(CXXFLAGS) -MM $^ > $(DEP)
	@sed -i 's|.*:|$(DIR_OBJ)/&|' $(DEP)

-include $(DEP)

# Compile C++
$(DIR_OBJ)/%.o: $(DIR_SRC)/%.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDES) $< -o $@

# Clean project
clean:
	$(RM) $(BIN)
	$(RM) $(DEP)
	$(RM) $(OBJ)
