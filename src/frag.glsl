#version 330

// Interpolated colors from the vertex shaders
in vec4 color;

// Output color to buffer
out vec4 fragColor;

void main()
{
	//fragColor = vec4(1.f);
	fragColor = color;
}
