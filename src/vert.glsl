#version 330

// Model-view-projection matrix
uniform mat4 mvp;

// Vertex co-ordinates
layout (location = 0) in vec2 position;
layout (location = 1) in float height;

// Output vertex color to fragment shader
out vec4 color;

// Matlab 'jet' gradient color scheme (assumes z already normalized to [0:1])
vec3 jetRGB(float z)
{
	vec3 rgb;
	rgb.r = (3.f * z) - 1.f;
	rgb.g = sin(3.14159f * z);
	rgb.b = (-3.f * z) + 2.f;
	return rgb;
}

void main()
{
	// Output position of the vertex, in clip space = MVP * position
	gl_Position = mvp * vec4(position, height, 1.f);

	// Calculate vertex color based on height
	color = vec4(jetRGB(height), 1.f);
	//color = vec4(position.x, vec3(1.f));
}
