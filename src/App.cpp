// Class header
#include "App.h"

// System headers
#include <cstdio>
#include <fstream>
#include <string>
#include <vector>

// Third-party headers
#include <GL/glew.h>	// Include before glut

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/freeglut.h>
#endif

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

// Project headers
#include "OpenCl.h"
#include "utils.h"

// Global pointer to help with registering member callback functions
App *pGlobalInstance = NULL;

App::App(int argc, char** argv)
{
	// Default window and application properties
	int window_x = -1;
	int window_y = -1;
	int window_w = 512;
	int window_h = 512;
	const char* window_title = "OpenCL-OpenGL Interoperability Test";
	numPoints = 256;

	// Parse command-line args
	if (utils::cmdLineArgExists(argc, argv, "-x")) {
		window_x = atoi(utils::getCmdLineArg(argc, argv, "-x"));
	}
	if (utils::cmdLineArgExists(argc, argv, "-y")) {
		window_y = atoi(utils::getCmdLineArg(argc, argv, "-y"));
	}
	if (utils::cmdLineArgExists(argc, argv, "-w")) {
		window_w = atoi(utils::getCmdLineArg(argc, argv, "-w"));
		window_w = window_w < 1 ? 1 : window_w;
	}
	if (utils::cmdLineArgExists(argc, argv, "-h")) {
		window_h = atoi(utils::getCmdLineArg(argc, argv, "-h"));
		window_h = window_h < 1 ? 1 : window_h;
	}
	if (utils::cmdLineArgExists(argc, argv, "-t")) {
		window_title = utils::getCmdLineArg(argc, argv, "-t");
	}
	if (utils::cmdLineArgExists(argc, argv, "-n")) {
		numPoints = atoi(utils::getCmdLineArg(argc, argv, "-n"));
		if (numPoints < 16) {
			numPoints = 16;
			utils::warning("Number of points must be >= 16");
		}
	}

	// Init GLUT and create window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowPosition(window_x, window_y);
	glutInitWindowSize(window_w, window_h);
	glutWindowHandle = glutCreateWindow(window_title);

	// Register callbacks
	registerCallbacks();

	// Initialize OpenGL stuff
	initGL();

	// Initialize OpenCL
	openCL = new OpenCl(numPoints);
	openCL->initGlBuffers(positionBuffer, heightBuffer);
	openCL->buildKernels();
}

App::~App()
{
	if (glutWindowHandle) {
		glutDestroyWindow(glutWindowHandle);
	}
}

void App::registerCallbacks()
{
	pGlobalInstance = this;
	glutReshapeFunc(reshapeCallback);
	glutSpecialFunc(specialCallback);
	glutMouseFunc(mouseCallback);
	glutMotionFunc(motionCallback);
	glutDisplayFunc(displayCallback);
	glutIdleFunc(idleCallback);
}

void App::initGL()
{
	// Initialize GLEW
	glewInit();

	//
	std::vector<glm::vec2> positions;
	std::vector<float> heights;

	//
	float zmin = 99;
	float zmax = -99;

	for (int i = 0; i < numPoints; i++) {
		for (int j = 0; j < numPoints; j++) {
			float x = (i - numPoints / 2) / (numPoints / 2.f);
			float y = (j - numPoints / 2) / (numPoints / 2.f);
			float t = hypotf(x, y) * 4.0;
			float z = (1 - t * t) * expf(t * t / -2.0);
			zmin = z < zmin ? z : zmin;
			zmax = z > zmax ? z : zmax;
			positions.push_back(glm::vec2(x, y));
			heights.push_back(z);
		}
	}

	// Normalize heights to [0:1]
	for (int i = 0; i < numPoints*numPoints; i++) {
		heights[i] = (heights[i] - zmin) / (zmax - zmin);
	}

	glGenBuffers(1, &positionBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glBufferData(GL_ARRAY_BUFFER, positions.size()*sizeof(glm::vec2), &positions[0], GL_STATIC_DRAW);

	glGenBuffers(1, &heightBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, heightBuffer);
	glBufferData(GL_ARRAY_BUFFER, heights.size()*sizeof(float), &heights[0], GL_DYNAMIC_DRAW);

	//
	//std::vector<GLushort> indices;

	// Build shader program
	std::vector<std::string> vertShaderFiles;
	std::vector<std::string> fragShaderFiles;
	vertShaderFiles.push_back("src/vert.glsl");
	fragShaderFiles.push_back("src/frag.glsl");
	programID = loadShaders(vertShaderFiles, fragShaderFiles);
	glUseProgram(programID);

	// Get attribute locations
	locationMVP = glGetUniformLocation(programID, "mvp");
	if (locationMVP == -1) {
		utils::error("Could not bind uniform attribute: mvp");
	}

	// Enable depth test (only accept fragment if it closer to the camera)
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	// Change point size
	//glPointSize(3.f);

	// Default MVP matrices values
	projectionMatrix = glm::perspective(45.f, 4.f / 3.f, 0.1f, 100.f); // 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	viewMatrix = glm::lookAt(glm::vec3(0, -2, 3),   // Camera position in World Space
							 glm::vec3(0, 0, 0),   // Camera looks at the origin
							 glm::vec3(0, 1, 0));  // Head is up (set to (0,-1,0) to look upside-down)
	translationMatrix = glm::mat4(1.f);
	rotationMatrix = glm::mat4(1.f);
	scaleMatrix = glm::mat4(1.f);
	modelMatrix = translationMatrix * rotationMatrix * scaleMatrix * glm::mat4(1.f);

	// Init translation, scaling, and rotation
	offsetX = 0.f;
	offsetY = 0.f;
	rotateX = 0.f;
	rotateY = 0.f;
}

GLuint App::loadShaders(const std::vector<std::string>& vertShaderFiles,
						const std::vector<std::string>& fragShaderFiles)
{
	// Create program object and list of shader IDs
	GLuint program_id = glCreateProgram();
	std::vector<GLuint> shader_ids;

	// Loop over vertex shaders
	for (auto it = vertShaderFiles.begin(); it != vertShaderFiles.end(); ++it) {
		// Create vertex shader
		GLuint vert_shader_id = glCreateShader(GL_VERTEX_SHADER);
		shader_ids.push_back(vert_shader_id);

		// Compile vertex shader
		const std::string source_code = readFile(*it);
		compileShader(vert_shader_id, source_code);
	}

	// Loop over fragment shaders
	for (auto it = fragShaderFiles.begin(); it != fragShaderFiles.end(); ++it) {
		// Create fragment shader
		GLuint frag_shader_id = glCreateShader(GL_FRAGMENT_SHADER);
		shader_ids.push_back(frag_shader_id);

		// Compile fragment shader
		const std::string source_code = readFile(*it);
		compileShader(frag_shader_id, source_code);
	}

	// Attach shaders to program
	for (auto it = shader_ids.begin(); it != shader_ids.end(); ++it) {
		glAttachShader(program_id, *it);
	}

	// Link and check program
	glLinkProgram(program_id);

	GLint result = GL_FALSE;
	GLint info_log_length;
	glGetProgramiv(program_id, GL_LINK_STATUS, &result);
	glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &info_log_length);
	if (info_log_length > 0){
		std::vector<char> error_message(info_log_length + 1);
		glGetProgramInfoLog(program_id, info_log_length, NULL, &error_message[0]);
		utils::error(&error_message[0]);
	}

	// Delete shaders
	for (auto it = shader_ids.begin(); it != shader_ids.end(); ++it) {
		glDeleteShader(*it);
	}

	return program_id;
}

void App::reshape(int w, int h)
{
	//  Reset viewport
	glViewport(0, 0, w, h);
}

void App::special(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_LEFT:
		offsetX -= 0.5f;
		break;
	case GLUT_KEY_RIGHT:
		offsetX += 0.5f;
		break;
	case GLUT_KEY_UP:
		offsetY += 0.5f;
		break;
	case GLUT_KEY_DOWN:
		offsetY -= 0.5f;
		break;
	case GLUT_KEY_HOME:
		offsetX = 0.f;
		offsetY = 0.f;
		rotateX = 0.f;
		rotateY = 0.f;
		rotationMatrix = glm::mat4(1.f);
		break;
	}

	translationMatrix = glm::translate(offsetX, offsetY, 0.f);
	//rotationMatrix = glm::rotate(offsetX, glm::vec3(1.f, 0.f, 0.f));
}

void App::mouse(int button, int state, int x, int y)
{
    // Handle mouse interaction for rotating the model
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		mousePressed = true;
	} else {
		mousePressed = false;
		//rotationMatrix = glm::mat4(1.f);  // Resets rotation once mouse released
	}

    mouseOldX = x;
    mouseOldY = y;
}

void App::motion(int x, int y)
{
    // Handle mouse interaction for rotating the model
	if (mousePressed) {
		const float dx = x - mouseOldX;
		const float dy = y - mouseOldY;
		rotateX += dy * 0.2;
		rotateY += dx * 0.2;	// Actually rotateZ now
		rotationMatrix = glm::rotate(rotateX, glm::vec3(1.f, 0.f, 0.f));
		rotationMatrix *= glm::rotate(rotateY, glm::vec3(0.f, 0.f, 1.f));

		// Reset mouse co-ordinates
		mouseOldX = x;
		mouseOldY = y;
	}
}


void App::display()
{
	// Clear colour and depth buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Update heights using OpenCL kernel
	openCL->update((float)glutGet(GLUT_ELAPSED_TIME));

	// Use shader program
	glUseProgram(programID);

	// Compute MVP matrix and bind it to uniform value in vertex shader
	modelMatrix = translationMatrix * rotationMatrix * scaleMatrix;
	glm::mat4 mvpMatrix = projectionMatrix * viewMatrix * modelMatrix;
	glUniformMatrix4fv(locationMVP, 1, GL_FALSE, &mvpMatrix[0][0]);

	// Bind position buffer
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glVertexAttribPointer(
		0,                  // attribute
		2,                  // elements per index (x,y)
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	// Bind height buffer
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, heightBuffer);
	glVertexAttribPointer(
		1,                                // attribute
		1,                                // elements per index (z)
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	// Draw buffers
	glDrawArrays(GL_POINTS, 0, numPoints*numPoints);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	// Display frames per second
	displayFPS();

	// Swap old and new framebuffers to update display
	glutSwapBuffers();
}

void App::idle()
{
	// Call display function (draw the current frame)
    glutPostRedisplay();
}

void App::run()
{
	// Enter GLUT event processing cycle
	glutMainLoop();
}


// Anonymous namespace
namespace
{
	extern "C" void reshapeCallback(int w, int h)
	{
		pGlobalInstance->reshape(w, h);
	}

	extern "C" void specialCallback(int key, int x, int y)
	{
		pGlobalInstance->special(key, x, y);
	}

	extern "C" void mouseCallback(int button, int state, int x, int y)
	{
		pGlobalInstance->mouse(button, state, x, y);
	}

	extern "C" void motionCallback(int x, int y)
	{
		pGlobalInstance->motion(x, y);
	}

	extern "C" void displayCallback()
	{
		pGlobalInstance->display();
	}

	extern "C" void idleCallback()
	{
		pGlobalInstance->idle();
	}

	void printText(const float x, const float y, const char* text)
	{
		// Fonts supported by GLUT are:
		// GLUT_BITMAP_8_BY_13
		// GLUT_BITMAP_9_BY_15
		// GLUT_BITMAP_TIMES_ROMAN_10
		// GLUT_BITMAP_TIMES_ROMAN_24
		// GLUT_BITMAP_HELVETICA_10
		// GLUT_BITMAP_HELVETICA_12
		// GLUT_BITMAP_HELVETICA_18

		// Text starting window co-ordinates (lower-left of text "box")
		glRasterPos2f(x, y);

		// Text color
		glColor3f(0.f, 1.f, 1.f);

		// Write text one letter at a time using specified font
		for (const char* c = text; *c != '\0'; c++) {
			glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *c);
		}
	}

	void displayFPS()
	{
		// Use static variables instead of globals
		static unsigned int frame = 0;
		static unsigned int time_prev = 0;
		static char fps_str[16] = "FPS:";

		// Update counters
		frame++;
		unsigned int time_curr = glutGet(GLUT_ELAPSED_TIME);

		// Calculate FPS
		if (time_curr - time_prev > 1000) {
			sprintf(fps_str, "FPS: %4.2f", frame * 1000.f / (time_curr - time_prev));
			time_prev = time_curr;
			frame = 0;
		}

		// Display FPS on-screen
		printText(-0.95f, 0.9f, fps_str);
	}

	std::string readFile(const std::string filename)
	{
		// Open file
		std::ifstream file(filename);
		if (!file.is_open()) {
			utils::error("Unable to open file: " + filename);
		}

		// Read file contents
		std::string file_contents(std::istreambuf_iterator<char>(file),
								 (std::istreambuf_iterator<char>()));
		return file_contents;
	}

	void compileShader(const GLuint shaderID, const std::string sourceCode)
	{
		// Compile shader
		const GLchar* source = sourceCode.c_str();
		glShaderSource(shaderID, 1, &source, NULL);
		glCompileShader(shaderID);

		// Check shader compilation status
		GLint result = GL_FALSE;
		GLint info_log_length;
		glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &info_log_length);
		if (info_log_length > 0) {
			std::vector<char> error_message(info_log_length + 1);
			glGetShaderInfoLog(shaderID, info_log_length, NULL, &error_message[0]);
			utils::error(&error_message[0]);
		}
	}
}
