#pragma once

// System headers
#include <string>
#include <vector>

// Third-party headers
#include "GL/glew.h"
#include "glm/glm.hpp"

// Project headers
#include "OpenCl.h"

// Main window application class
class App {
public:
	// Constuctors and destructors
	App(int argc, char** argv);
	~App();

	// Mutators
	void reshape(int w, int h);
	void special(int key, int x, int y);
	void mouse(int button, int state, int x, int y);
	void motion(int x, int y);
	void display();
	void idle();
	void run();

	// Accessors
private:
	// Variables
	int glutWindowHandle;
	int numPoints;
	GLuint programID;
	GLint locationMVP;
	GLuint positionBuffer;
	GLuint heightBuffer;

	float offsetX;
	float offsetY;
	float rotateX;
	float rotateY;

	bool mousePressed;
	int mouseOldX;
	int mouseOldY;

	glm::mat4 projectionMatrix;
	glm::mat4 viewMatrix;

	glm::mat4 translationMatrix;
	glm::mat4 rotationMatrix;
	glm::mat4 scaleMatrix;
	glm::mat4 modelMatrix;

	OpenCl* openCL;

	// Mutators
	void registerCallbacks();
	void initGL();
	GLuint loadShaders(const std::vector<std::string>& vertShaders,
					   const std::vector<std::string>& fragShaders);

	// Accessors
};

// Anonymous namespace
namespace
{
	extern "C" void reshapeCallback(int w, int h);
	extern "C" void specialCallback(int key, int x, int y);
	extern "C" void mouseCallback(int button, int state, int x, int y);
	extern "C" void motionCallback(int x, int y);
	extern "C" void displayCallback();
	extern "C" void idleCallback();
	void printText(const float x, const float y, const char* text);
	void displayFPS();
	std::string readFile(const std::string filename);
	void compileShader(const GLuint shaderID, const std::string sourceCode);
}
