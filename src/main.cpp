// Project headers
#include "App.h"

int main(int argc, char** argv)
{
	App mainApp(argc, argv);
	mainApp.run();

	return 0;
}
