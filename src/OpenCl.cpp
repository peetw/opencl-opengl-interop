// Class header
#include "OpenCl.h"

// System headers
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// Third-party headers
#include <CL/cl.hpp>
#include <GL/glx.h>

// Project headers
#include "utils.h"

OpenCl::OpenCl(const unsigned int numPoints)
{
	try {
		// Select specified platform
		std::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);
		platform = platforms[0];

		// Find specified device type
		std::vector<cl::Device> devices;
		platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
		device = devices[0];

		// Create context and command queue
		//cl_context_properties context_props[] = {
			//CL_CONTEXT_PLATFORM, (cl_context_properties)(platforms[platformIdx])(),
			//0
		//};

		std::cout << "glxContext: " << (cl_context_properties)glXGetCurrentContext() << "\n";
		std::cout << "glxDisplay: " << (cl_context_properties)glXGetCurrentDisplay() << "\n";

		cl_context_properties context_props[] = {
			CL_CONTEXT_PLATFORM, (cl_context_properties)platform(),
			CL_GL_CONTEXT_KHR, (cl_context_properties)glXGetCurrentContext(),
			CL_GLX_DISPLAY_KHR, (cl_context_properties)glXGetCurrentDisplay(),
			0
		};
		context = cl::Context(devices, context_props);
		queue = cl::CommandQueue(context, device);
	} catch(cl::Error &e) {
		utils::clError(e, __FUNCTION__);
	}

	// Display selected device and its details
	//printDetails();

	// Set kernel work sizes
	globalWorkSz = cl::NDRange(numPoints, numPoints);
	localWorkSz = cl::NullRange;
}

void OpenCl::printDetails()
{
	std::cout << "\nOpenCL platform" << "\n-----------------------------------\n";
    std::cout << "Name:       " << platform.getInfo<CL_PLATFORM_NAME>() << "\n";
    std::cout << "Vendor:     " << platform.getInfo<CL_PLATFORM_VENDOR>() << "\n";
    std::cout << "Version:    " << platform.getInfo<CL_PLATFORM_VERSION>() << "\n";
    std::cout << "Profile:    " << formatProfile(platform.getInfo<CL_PLATFORM_PROFILE>()) << "\n";
    std::cout << "Extensions: " << platform.getInfo<CL_PLATFORM_EXTENSIONS>() << "\n";

    std::cout << "\nOpenCL device" << "\n-----------------------------------\n";
    std::cout << "Name:          " << device.getInfo<CL_DEVICE_NAME>() << "\n";
    std::cout << "Type:          " << formatDeviceType(device.getInfo<CL_DEVICE_TYPE>()) << "\n";
    std::cout << "Vendor:        " << device.getInfo<CL_DEVICE_VENDOR>() << "\n";
    std::cout << "Version:       " << device.getInfo<CL_DEVICE_VERSION>() << "\n";
    std::cout << "Profile:       " << formatProfile(device.getInfo<CL_DEVICE_PROFILE>()) << "\n";
    std::cout << "Compute units: " << device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << "\n";
    std::cout << "Clock freq:    " << formatFreq(device.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>()) << "\n";
    std::cout << "Global mem:    " << formatMemSize(device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>()) << "\n";
    std::cout << "Constant mem:  " << formatMemSize(device.getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>()) << "\n";
    std::cout << "Local mem:     " << formatMemSize(device.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>()) << "\n";
    std::cout << "Extensions:    " << device.getInfo<CL_DEVICE_EXTENSIONS>() << "\n";
}

void OpenCl::initGlBuffers(const GLuint positionBuffer, const GLuint heightBuffer)
{
	printf("posID: %u   heightID: %u\n", positionBuffer, heightBuffer);
	try {
		// Initialize device buffers
		glFinish();
		openGlBuffers.push_back(cl::BufferGL(context, CL_MEM_READ_ONLY, positionBuffer, NULL));
		openGlBuffers.push_back(cl::BufferGL(context, CL_MEM_READ_WRITE, heightBuffer, NULL));
	} catch(cl::Error &e) {
		utils::clError(e, __FUNCTION__);
	}
}

void OpenCl::buildKernels()
{
	// pre_hydo.cl
	std::string file = "src/update_heights.cl";
	updateHeights = buildKernel(file, "updateHeights");
}

cl::Kernel OpenCl::buildKernel(const std::string &filename,
		const std::string &funcName)
{
	cl::Kernel kernel;
	try {
		// Read source file
		std::ifstream sourceFile(filename);
		if (!sourceFile.is_open()) {
			utils::error("Unable to open file: " + filename);
		}
		std::string sourceCode(std::istreambuf_iterator<char>(sourceFile),
				(std::istreambuf_iterator<char>()));
		cl::Program::Sources source(1, std::make_pair(sourceCode.c_str(),
				sourceCode.length()+1));

		// Make program of the source code in the context
		cl::Program program = cl::Program(context, source);

		// Build program for the specified device
		std::vector<cl::Device> devices;
		devices.push_back(device);
		try {
			program.build(devices, "-I src");
		} catch(cl::Error &e) {
			utils::error("Failed to build kernel: " + funcName +
					"\nBuild log:\n" +
					program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device));
		}

		// Make kernel
		kernel = cl::Kernel(program, funcName.c_str());
	} catch(cl::Error &e) {
		utils::clError(e, __FUNCTION__);
	}
	return kernel;
}

void OpenCl::setKernelArgs(cl_float time)
{
	try {
		// update_heights.cl
		updateHeights.setArg(0, openGlBuffers[0]);
		updateHeights.setArg(1, openGlBuffers[1]);
		updateHeights.setArg(2, sizeof(cl_float), &time);
	} catch(cl::Error &e) {
		utils::clError(e, __FUNCTION__);
	}
}




void OpenCl::update(cl_float time)
{
	try {
		// Make sure OpenGL has finished with the buffers
		glFinish();
		queue.enqueueAcquireGLObjects(&openGlBuffers, NULL, NULL);
		queue.finish();

		// Set kernel args
		setKernelArgs(time);

		// Update heights
		queue.enqueueNDRangeKernel(updateHeights, cl::NullRange,
				globalWorkSz, localWorkSz);
		queue.finish();

		// Release buffers so that OpenGL can use them
		queue.enqueueReleaseGLObjects(&openGlBuffers, NULL, NULL);
		queue.finish();
	} catch(cl::Error &e) {
		utils::clError(e, __FUNCTION__);
	}
}

void OpenCl::finish()
{
	try {
		queue.finish();
	} catch(cl::Error &e) {
		utils::clError(e, __FUNCTION__);
	}
}

// Anonymous namespace
namespace
{
std::string formatProfile(const std::string profile)
{
	std::string formatted_profile = "Unknown";
	if (profile == "FULL_PROFILE") {
		formatted_profile = "Full";
	} else if (profile == "EMBEDDED_PROFILE") {
		formatted_profile = "Embedded";
	}
	return formatted_profile;
}

std::string formatDeviceType(const cl_device_type type_enum)
{
	std::string type = "Unknown";
	if (type_enum == CL_DEVICE_TYPE_DEFAULT) {
		type = "Default";
	} else if (type_enum == CL_DEVICE_TYPE_CPU) {
		type = "CPU";
	} else if (type_enum == CL_DEVICE_TYPE_GPU) {
		type = "GPU";
	} else if (type_enum == CL_DEVICE_TYPE_ACCELERATOR) {
		type = "Accelerator";
	}
	return type;
}

std::string formatFreq(const cl_uint freq)
{
	char formatted_freq[64] = "Unkown clock frequency";
	if (freq > 0 && freq < 1000) {
		sprintf(formatted_freq, "%u MHz", freq);
	} else if (freq >= 1000 && freq < 1000*1000) {
		sprintf(formatted_freq, "%.3g GHz", freq / 1000.);
	}
	return formatted_freq;
}

std::string formatMemSize(const cl_ulong mem_size)
{
	char formatted_mem_size[64] = "Unkown memory size";
	if (mem_size > 0 && mem_size < 1024) {
		sprintf(formatted_mem_size, "%lu B", mem_size);
	} else if (mem_size >= 1024 && mem_size < 1024*1024) {
		sprintf(formatted_mem_size, "%.3g kB", mem_size / 1024.);
	} else if (mem_size >= 1024*1024 && mem_size < 1024*1024*1024) {
		sprintf(formatted_mem_size, "%.3g MB", mem_size / (1024.*1024.));
	} else if (mem_size >= 1024*1024*1024) {
		sprintf(formatted_mem_size, "%.3g GB", mem_size / (1024.*1024.*1024.));
	}
	return formatted_mem_size;
}
}	// End anonymous namespace
