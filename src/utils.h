#pragma once
#define __CL_ENABLE_EXCEPTIONS

// System headers
#include <string>

// Third-party headers
#include <CL/cl.hpp>

namespace utils {
bool cmdLineArgExists(int argc, char** argv, const std::string &arg);
char* getCmdLineArg(int argc, char** argv, const std::string &arg);
void warning(const std::string &msg);
void error(const std::string &msg);
void clError(const cl::Error &e, const char* func);
std::string openClErrorStr(const cl_int err);
}
