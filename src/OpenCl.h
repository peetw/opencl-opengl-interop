#pragma once
#define __CL_ENABLE_EXCEPTIONS

// System headers
#include <string>
#include <vector>

// Third-party headers
#include <CL/cl.hpp>

// Project headers

// OpenCL handler class
class OpenCl {
public:
	// Constuctors
	OpenCl(const unsigned int numPoints);

	// Mutators

	// Accessors
	void initGlBuffers(const GLuint positionBuffer, const GLuint heightBuffer);
	void buildKernels();
	void update(cl_float time);
	void finish();
private:
	// -----------------------------------------------------------------------
	// VARIABLES
	// -----------------------------------------------------------------------

	// --------------------------------
	// Device
	// --------------------------------

	// OpenCL device variables
	cl::Platform platform;
	cl::Device device;
	cl::Context context;
	cl::CommandQueue queue;

	// Kernel launch parameters
	cl::NDRange globalWorkSz;
	cl::NDRange localWorkSz;

	// --------------------------------
	// Buffers
	// --------------------------------

	std::vector<cl::Memory> openGlBuffers;

	// --------------------------------
	// Kernels
	// --------------------------------

	// update_heights.cl
	cl::Kernel updateHeights;

	// -----------------------------------------------------------------------
	// ACCESSORS
	// -----------------------------------------------------------------------
	void printDetails();

	// -----------------------------------------------------------------------
	// MUTATORS
	// -----------------------------------------------------------------------
	cl::Kernel buildKernel(const std::string &filename,
						   const std::string &funcName);
	void setKernelArgs(cl_float time);
};


// Anonymous namespace
namespace
{
std::string formatProfile(const std::string profile);
std::string formatDeviceType(const cl_device_type type_enum);
std::string formatFreq(const cl_uint freq);
std::string formatMemSize(const cl_ulong mem_size);
}
